import {Link} from "react-router-dom"
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>

            <Typography variant="h5">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI : &nbsp;
            </Typography>
            
                <Link to="/">
                    <Typography variant="body1" >
                        เครื่องคิดเลข
                    </Typography>
                </Link>
                &nbsp; &nbsp; &nbsp;

                <Link to="about">
                    <Typography variant="body1">
                        ผู้จัดทำ
                    </Typography>
                </Link>
                &nbsp; &nbsp; &nbsp;

                <Link to="/contact">
                    <Typography variant="body1">
                        ติดต่อ
                    </Typography>
                </Link>
                &nbsp; &nbsp; &nbsp;

                <Link to="/LuckyNumber">
                    <Typography variant="body1">
                        สุ่มเลข
                    </Typography>
                </Link>

        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;

/*function Header() {

    return (
        <div align = "left">
            <p>ยินดีต้อนรับสู่เว็บคำนวณ BMI : &nbsp;
                <Link to="/">เครื่องคิดเลข</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="about">ผู้จัดทำ</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/contact">ติดต่อ</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/LuckyNumber">สุ่มเลข</Link>
            </p>
        </div>
    );
}

export default Header;*/