import './AboutUs.css'
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
//const ABoutUs = () = =>{}
function AboutUs (props){


    return (
        <Box sx={ {width: "60%}"}}>
            <Paper elevation = {3}>
                <br />
                <h2> Developed by { props.name } </h2>
                <h3> He is from { props.province }.</h3>
                <br />
            </Paper>
        </Box>
    );
}

export default AboutUs;