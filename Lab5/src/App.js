import './App.css';
import { Routes, Route } from "react-router-dom";
import Header from './components/Header'
import AboutUsPage from './pages/AboutUsPage';
import BMICalPage from './pages/BMICalPage';
import ContactPage from './pages/ContactPage'
import LuckyNumberPage from './pages/LuckyNumberPage'

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="about" element ={
              <AboutUsPage/>
          }/>

          <Route path="/" element ={
              <BMICalPage/>
          }/>

          <Route path="/contact" element ={
              <ContactPage/>
          }/>

          <Route path="/LuckyNumber" element ={
              <LuckyNumberPage/>
          }/>
      </Routes>
    </div>
  );
}

export default App;
