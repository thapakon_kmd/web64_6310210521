import BMIResult from '../components/BMIResult.js'
import {useState} from "react";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from '@mui/material';



function BMICalPage() {

    const [ name, setName ] = useState("");
    const [ bmiResult, setBmiResult ] = useState("");
    const [ TranslateResult, setTranslateResult ] = useState("");

    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w/(h*h);
        setBmiResult(bmi);
        if(bmi>25){
            setTranslateResult("อ้วนนะเราอะ");
        }else if(bmi<20){
            setTranslateResult("กินเยอะๆ");
        }else{
            setTranslateResult("หุ่นดีเหลือเกินนนนน เจ้าพ่อคูณณณณณณณณณณ");
        }
    }

    return(
        <Container maxWidth = 'lg'>
            <Grid container spacing={2} sx={{ marginTop : "10px" }}>

                <Grid item xs={12}>
                    <Typography variant = "h5">ยินดีต้อนรับสู่เว็บคำนวณ BMI</Typography>
                </Grid>

                <Grid item xs={6}>
                    <Box sx={{ textAlign : 'left'}}>
                        <br/>
                        <h3>ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
                        </h3>
                        <h3>ส่วนสูง (m) : <input type = "text" value={height} onChange={(e) =>{setHeight(e.target.value);}}/>
                        </h3>

                        <h3>น้ำหนัก (kg) : <input type = "text" value={weight} onChange={(e) => {setWeight(e.target.value);}}/></h3>
                        <br />
                        <Button variant="contained" onClick={() => {calculateBMI()}}> Calculate </Button>
                    </Box>
                </Grid>

                <Grid item xs={5}>
                    <Box sx={{ textAlign : 'center'}}>
                        { bmiResult != 0 && 
                            <div>
                                <Typography variant = "h6">ผลการคำนวณอยู่นี่จ้าาาาาา</Typography>
                                <BMIResult
                                    name ={ name }
                                    BMI = { bmiResult }
                                    result = {TranslateResult}
                                />
                            </div>
                        }
                    </Box>
                </Grid>  
                
            </Grid>
        </Container>
    );
}

export default BMICalPage;

/*
<div align = "left">
        <div align = "center">
            <br/>
            <h1>ยินดีต้อนรับสู่เว็บคำนวณ BMI</h1>
            <br/>

            <h2>ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
            </h2>

            <br/>
            <h3>ส่วนสูง (m) : <input type = "text" value={height} onChange={(e) =>{setHeight(e.target.value);}}/>
            </h3>

            <h3>น้ำหนัก (kg) : <input type = "text" value={weight} onChange={(e) => {setWeight(e.target.value);}}/></h3>
            <br />
            <Button variant="contained" onClick={() => {calculateBMI()}}> Calculate </Button>

            <br/>
            <br/>
            <br/>

            { bmiResult != 0 && 
                <div>
                    <h3>ผลการคำนวณอยู่ตรงนี้จ้าาาาา</h3>
                    <BMIResult
                        name ={ name }
                        BMI = { bmiResult }
                        result = {TranslateResult}
                    />
                </div>
            }
                    
        </div>
    </div>
*/