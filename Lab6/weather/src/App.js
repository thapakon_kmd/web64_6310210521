import logo from './logo.svg';
import './App.css';

import {Box,AppBar, Toolbar, Typography, Card } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';

import {useEffect, useState} from 'react';
import axios from 'axios';
import ReactWeather, { useOpenWeather } from 'react-open-weather';

function App() {

  const [temperature, setTemperature ]  = useState();
  //OpenWeatherMap API key bed41100d5853f8575be36ead02926cb

  const weatherAPIUrl =
                "http://api.openweathermap.org/data/2.5/weather?" ;
  const city = "Songkhla";
  const apiKey = "bed41100d5853f8575be36ead02926cb" ;

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: 'bed41100d5853f8575be36ead02926cb',
    lat: '7.1898',
    lon: '100.5954',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });

  useEffect( () => {
      setTemperature("-------");

      axios.get(weatherAPIUrl + "q=" + city+"&appid="+apiKey).then ( (response)=> {
        let data = response.data;
        let temp = data.main.temp - 273;
        setTemperature(temp.toFixed(2));
      })
    }
  , [] );
  return (
    <Box sx={{ flexGrow : 1, width : "100%" }}>
          <AppBar position="static">
              <Toolbar>
                <Typography variant="h6">
                  Weather App
                </Typography>
              </Toolbar>
          </AppBar>

          <Box sx = {{ justifyContent : 'center', marginTop : "20px", width: "100%" ,display: "flex"}}>
              <Typography variant="h4" sx = {{ fontFamily : "Sukhumvit Set", fontWeight: "bold" }}>
                อากาศหาดใหญ่วันนี้
              </Typography>
          </Box>

          <Box sx = {{ justifyContent : 'center', marginTop : "20px", width: "100%" ,display: "flex"}}>
              <Card sx={{ minWidth: 400}}>
                      <CardContent>
                          <Typography sx={{ fontSize: 16, fontFamily : "Sukhumvit Set" }}>
                            หาดใหญ่, สงขลา
                          </Typography>
                              <Typography variant="h5" component="div">
                                  { temperature }
                              </Typography>
                              <Typography variant="p" component="div" sx={{ marginTop: "10px", fontFamily : "Sukhumvit Set" }} color="text.secondary" gutterBottom>
                                  อากาศแจ่มใส หัวใจชื่นบาน
                              </Typography>
                                  
                      </CardContent>
              </Card>
              <ReactWeather
                  isLoading={isLoading}
                  errorMessage={errorMessage}
                  data={data}
                  lang="en"
                  locationLabel="Hat Yai"
                  unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
                  showForecast
                />
          </Box>
          <div class = "footer">
              <p>Developed by TK23</p>
          </div>
    </Box>
  );
}

export default App;
