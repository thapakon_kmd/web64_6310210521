import './App.css';
import Header from "./components/Header"
import Footer from "./components/Footer"
import Body from './components/Body';
import BodyforContact from './components/BodyforContact';


function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <BodyforContact/>
      <Footer />
    </div>
  );
}

export default App;
