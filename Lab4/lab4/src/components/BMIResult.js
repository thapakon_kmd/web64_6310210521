function BMIResult (props){

    return (

        <div id = "BG">
            <h3>You : {props.name} </h3>
            <h3>BMI : {props.BMI} </h3>
            <h3>Result : {props.result} </h3>
        </div>
    );
}
export default BMIResult;