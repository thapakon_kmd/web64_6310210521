const express = require('express')
const res = require('express/lib/response')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/triangle', (req,res)=>{
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)

    if (!isNaN(height) && !isNaN(base)){
        let area =  (1/2) * base * height
        
        res.send('Area is '+area+'.')

    }else{
        res.send('Try again')
    }
})

app.post('/score', (req, res) => {
    let name = 'Thapakon'
    let score = 80
        if (score>=80){
            res.send(name +' gets '+score+' points and your Grade is A.')
        }else if(score>=75){
            res.send(name +' gets '+score+' points and your Grade is B+.')
        }else if(score>=70){
            res.send(name +' gets '+score+' points and your Grade is B.')
        }else if(score>=65){
            res.send(name +' gets '+score+' points and your Grade is C+.')
        }else if(score>=60){
            res.send(name +' gets '+score+' points and your Grade is C.')
        }else if(score>=55){
            res.send(name +' gets '+score+' points and your Grade is D+.')
        }else if(score>=50){
            res.send(name +' gets '+score+' points and your Grade is D.')
        }else{
            res.send(name +' gets '+score+' failed to get any grade.')
        }
})

app.post('/bmi', (req, res) => {

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)

    if (!isNaN(weight) && !isNaN(height)){
        let bmi = weight / (height * height)

        result = {
            "status" : 200,
            "bmi"    : bmi,
        }
    }else{
        result = {
            "status" : 400,
            "message" : "Weight or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})